import 'core-js/fn/object/entries';

import { 
  HAS_ERROR, 
  IS_SUBMITED,
  DATA_SUCCESS_MSG, 
  DATA_RULES 
} from '../shared/settings';

class FormValidation {
  constructor(scopeSelector, elementToValidateSelector, formControlsSelector, submitSelector) {
    this.scope                = document.querySelector(scopeSelector);
    this.form                 = this.scope ? this.scope.querySelector('form') : '';
    this.inputs               = this.scope ? this.scope.querySelectorAll(elementToValidateSelector) : '';
    this.formControlsSelector = formControlsSelector;
    this.submitSelector       = submitSelector;
  }

  init() {
    if (!this.check())
      return;
    
    this.bindEvents();
  }

  bindEvents() {
    const _this = this;

    _this.addAttributes();

    _this.form.addEventListener('submit', function(e) {
      if(!_this.isSubmitted(e))
        return;

      _this.handler(e, _this);
    });
  }

  handler(e, self) {
    const _this = self;

    e.preventDefault();

    _this.validate();
  }

  addAttributes() {
    this.inputs.forEach(input => {
      const data = input.dataset[DATA_RULES];

      if (data) {
        const parsedObj = JSON.parse(data);

        for (const [key, value] of Object.entries(parsedObj)) {
          input.setAttribute(`${key}`, `${value}`);
        }
      }
    });
  }

  validate() {
    const _this = this;

    this.inputs.forEach(input => {
      const jsControls = input.closest(_this.formControlsSelector);

      if (!input.checkValidity()) {
        input.nextElementSibling.innerHTML = input.validationMessage;

        jsControls.classList.add(HAS_ERROR);
      } else {
        jsControls.classList.remove(HAS_ERROR);
        _this.sendForm();
      }
    });
  }

  sendForm() {
    if (!this.form.checkValidity())
      return;
      
    // this.form.submit();
    const submit     = this.scope.querySelector(this.submitSelector);
    const successMsg = submit.dataset[DATA_SUCCESS_MSG];
    
    submit.innerHTML = successMsg;
    this.form.classList.add(IS_SUBMITED);

    setTimeout(() => {
      this.form.reset();
    }, 0);
  }

  isSubmitted(e) {
    if(e.target.classList.contains(IS_SUBMITED))
      return false;
    
    return true;
  }

  check() {
    if (!this.scope)
      return false;

    return true;
  }
}

const formValidation = new FormValidation('.js-validate-scope', '.js-validate-input', '.js-form-controls', '.js-submit');

formValidation.init();
