<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Form js component</title>
  <?php require_once($_SERVER['DOCUMENT_ROOT'] . '/scripts/get_bundle_name.php') ?>

  <link rel="stylesheet" href="<?php echo get_bundle_name()['css'] ?>">
</head>
<body>
  <div class="o-site">
    <div class="o-flex">
      <h1 class="t1 u-mt u-midnight-color">Js form <br>validation component</h1>
      <div class="c-form js-validate-scope u-mb">
        <div class="o-container">
          <form action="" novalidate>
            <div class="c-form__inner">
              <div class="c-form__group">
                <div class="c-form__controls js-form-controls">
                  <label for="name" class="c-form__label">Name</label>
                  <input class="js-validate-input" required="required" minlength="3" id="name" type="text">
                  <span class="c-form__help">Error</span>
                </div>
              </div>
              <div class="c-form__group">
                <div class="c-form__controls js-form-controls">
                  <label for="email" class="c-form__label">Email</label>
                  <input class="js-validate-input" required="required" type="email" id="email" type="text">
                  <span class="c-form__help">Error</span>
                </div>
              </div>
              <div class="c-form__group">
                <div class="c-form__controls js-form-controls">
                  <label for="msg" class="c-form__label">Message</label>
                  <textarea class="js-validate-input" 
                    data-rules='{&quot;required&quot;:&quot;required&quot;, &quot;minlength&quot;:&quot;3&quot;}' 
                    id="msg"></textarea>
                  <span class="c-form__help">Error</span>
                </div>
              </div>
              <div data-success-msg="Thanks, we'll responde soon (:" class="c-form__group c-form__action js-submit">
                <button class="c-btn" onclick="javascript:void(0)">Submit</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <script src="<?php echo get_bundle_name()['js'] ?>"></script>
</body>
</html>